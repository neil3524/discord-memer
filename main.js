const Canvas = require("canvas");
const Discord = require("discord.js");
const cl = new Discord.Client({ intents: [Discord.Intents.FLAGS.GUILDS, Discord.Intents.FLAGS.GUILD_MESSAGES] });
cl.commands = new Discord.Collection();

let canvas;
let ctx;

class MemeFormat {
    constructor(src, textLocations) {
        this.src = src;
        this.textLocations = textLocations;
    }
}

const memeFormats = {
    distractedboyfriend: new MemeFormat("distractedboyfriend.jpg", [
        [0.3, 0.75, 0.35],
        [0.6, 0.5, 0.27],
        [0.825, 0.55, 0.15],
    ]),
    expandingbrain: new MemeFormat("expandingbrain.jpg", [
        [0.25, 0.125, 0.45],
        [0.25, 0.375, 0.45],
        [0.25, 0.625, 0.45],
        [0.25, 0.875, 0.45],
    ]),
};

function getMemeFormatUsingName(string) {
    return memeFormats[Object.keys(memeFormats).find((key) => key === string)];
}

function addInteractions() {
    function getOptions(memeFormat) {
        let opts = [];
        for (let i = 0; i < memeFormats[memeFormat].textLocations.length; i++) {
            opts.push({
                name: `text${i + 1}`,
                description: `Text #${i + 1}`,
                required: true,
                type: 3,
            });
        }
        console.log("opts created: " + JSON.stringify(opts));
        return opts;
    }

    Object.keys(memeFormats).forEach((memeFormatName) => {
        cl.api
            .applications(cl.user.id)
            .guilds("726614427746435216")
            .commands.post({
                data: {
                    name: memeFormatName,
                    description: `Generates a ${memeFormatName} meme`,
                    options: getOptions(memeFormatName),
                },
            });
    });
}

cl.once("ready", () => {
    // addInteractions();
    // cl.channels.fetch("726647117703282729").then((channel) => channel.send("message"));
});
cl.on("messageCreate", generate);
cl.on("interactionCreate", async (interaction) => {
    if (interaction.isCommand()) {
        if (getMemeFormatUsingName(interaction.commandName)) {
            await interaction.deferReply({ ephemeral: true });
            // const deferTimeout = setTimeout(() => interaction.deferReply({ ephemeral: true }), 2750);
            generateWithInteraction(interaction).then(async (attachment) => {
                // clearTimeout(deferTimeout);
                const message = await interaction.channel.send({ content: `${interaction.user}`, files: [attachment] });
                const row = new Discord.MessageActionRow().addComponents(
                    new Discord.MessageButton().setLabel("Delete Meme").setStyle("DANGER").setCustomId(`${message.id}`)
                );
                await interaction.editReply({ content: "Delete meme generated?", components: [row] });
            });
        }
    } else if (interaction.isButton()) {
        interaction.channel.messages.fetch(interaction.customId).then((message) => {
            message.delete();
        });
        interaction.reply({ content: "Meme deleted.", ephemeral: true });
    }
});

function generate(msg) {
    if (!msg.content.startsWith("..")) return;

    let memeSelectionText = msg.content.split(" ")[0].substring(2); // first word of the user's command

    // msg.channel.send("message: " + memeSelectionText);
    // find selection in memeFormats
    let memeFormat = getMemeFormatUsingName(memeSelectionText);
    if (!memeFormat) {
        msg.channel.send(memeSelectionText + " does not exist.");
    } else {
        loadPic(memeFormat, msg.content).then((message) => {
            msg.channel.send({ files: [message] });
        });
    }
}

function generateWithInteraction(interaction) {
    return new Promise(async (resolve, reject) => {
        let memeFormat = getMemeFormatUsingName(interaction.commandName);
        let text = interaction.commandName + " ";
        interaction.options.data.forEach((opt, idx) => {
            if (idx + 1 !== interaction.options.data.length) {
                text += `${opt.value} | `;
            } else {
                text += `${opt.value}`;
            }
        });
        resolve(loadPic(memeFormat, text));
    });
}

// main reason for this class is to convert the percentage measurements to pixels
class CanvasMemeText {
    constructor(text, x, y, widthLimit) {
        this.text = text;
        this.x = canvas.width * x;
        this.y = canvas.height * y + parseInt(ctx.font) / 2;
        this.widthLimit = widthLimit * canvas.width;
    }
}

function loadPic(memeFormat, text) {
    return new Promise((resolve, reject) => {
        text = text.substring(text.indexOf(" ") + 1); // removes meme selection ex: 'distractedboyfriend'
        let texts = text.split(/[|\\]+/g);
        texts.forEach((text, i) => {
            texts[i] = texts[i].trim();
        });

        console.log(JSON.stringify(texts));

        let image = new Canvas.Image();
        image.onload = () => {
            // canvas.width = image.naturalWidth;
            // canvas.height = image.naturalHeight;
            canvas = Canvas.createCanvas(image.naturalWidth, image.naturalHeight);
            ctx = canvas.getContext("2d");
            ctx.drawImage(image, 0, 0);

            let memeTexts = [];
            for (let i = 0; i < memeFormat.textLocations.length; i++) {
                memeTexts.push(
                    new CanvasMemeText(texts[i] || "", memeFormat.textLocations[i][0], memeFormat.textLocations[i][1], memeFormat.textLocations[i][2])
                );
            }
            addText(memeTexts);

            console.log("Sending canvas...");
            resolve(new Discord.MessageAttachment(canvas.toBuffer(), "meme.png"));
        };
        image.onerror = (err) => console.error(err);
        image.src = memeFormat.src;
    });
}

function addText(memeTexts) {
    const padding = 4;
    let fontSize;
    ctx = canvas.getContext("2d");
    ctx.strokeStyle = "black";
    ctx.miterLimit = 5;
    ctx.lineWidth = 6;
    ctx.fillStyle = "white";

    memeTexts.forEach((memeText, idx) => {
        console.log("Adding text: " + memeText.text);

        let lines = [];

        ctx.font = "42px Impact";
        let textWidth = ctx.measureText(memeText.text).width;
        let counter = 1;

        let multilineTextPasted = false;
        while (textWidth > memeText.widthLimit) {
            ctx = canvas.getContext("2d");
            ctx.font = parseInt(ctx.font) - 2 + ctx.font.substring(2);
            console.log("Decreasing font size: " + ctx.font + "\tCount: " + counter);
            textWidth = ctx.measureText(memeText.text).width;
            if (counter++ >= 12) {
                // make new lines and paste on canvas
                let text = memeText.text;
                console.log("text.length", text.length);

                // word wrapping calculation
                while (text.length) {
                    let i, j;

                    // find where text should be cut off
                    for (i = text.length; ctx.measureText(text.substr(0, i)).width > memeText.widthLimit; i--);
                    const result = text.substr(0, i);

                    // cut off the text at the last space
                    if (i !== text.length) {
                        j = 0;
                        while (result.indexOf(" ", j) !== -1) {
                            j = result.indexOf(" ", j) + 1;
                        }
                    }

                    lines.push(result.substr(0, j || result.length));
                    console.log("lines.push: ", result.substr(0, j || result.length));
                    text = text.substr(lines[lines.length - 1].length, text.length);
                }

                // Render
                console.log("lines", lines);
                lines.forEach((line, i) => {
                    const textWidthCenter = ctx.measureText(line).width / 2;
                    console.log("textWidthCenter", textWidthCenter);
                    fontSize = parseInt(ctx.font);
                    const textHeightCenter = ((fontSize + padding) * lines.length) / 2;
                    ctx.strokeText(line, memeText.x - textWidthCenter, memeText.y + (fontSize + padding) * i + textHeightCenter);
                    ctx.fillText(line, memeText.x - textWidthCenter, memeText.y + (fontSize + padding) * i + textHeightCenter);
                });

                multilineTextPasted = true;

                break;
            }
        }

        if (!multilineTextPasted) {
            fontSize = parseInt(ctx.font);
            const textWidthCenter = ctx.measureText(memeText.text).width / 2;
            const textHeightCenter = (fontSize + padding) / 2;
            ctx.strokeText(memeText.text, memeText.x - textWidthCenter, memeText.y + textHeightCenter);
            ctx.fillText(memeText.text, memeText.x - textWidthCenter, memeText.y + textHeightCenter);
        }
    });
}

cl.login("ODc3MzEwNDk3MTEwNzY1NTk4.YRwxEg.lCL_xc3x1as-vX-5vgQ0_QKmYNs").then(() => {
    cl.user.setStatus("online");
});
